<libosinfo version="0.0.1">
<!-- Licensed under the GNU General Public License version 2 or later.
     See http://www.gnu.org/licenses/ for a copy of the license text -->
  <os id="http://redhat.com/rhel/6.10">
    <short-id>rhel6.10</short-id>
    <_name>Red Hat Enterprise Linux 6.10</_name>
    <version>6.10</version>
    <_vendor>Red Hat, Inc</_vendor>
    <family>linux</family>
    <distro>rhel</distro>
    <codename>Santiago</codename>
    <upgrades id="http://redhat.com/rhel/6.9"/>
    <derives-from id="http://redhat.com/rhel/6.9"/>

    <release-date>2018-05-25</release-date>
    <eol-date>2024-06-30</eol-date>

    <media arch="i686">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>RHEL-6.10 .*.i386$</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>RHEL-6.10 .*.x86_64$</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="ppc">
      <iso>
        <system-id>PPC</system-id>
        <volume-id>RHEL-6.10 .*.ppc64$</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="s390x">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>RHEL-6.10 .*.s390x$</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <resources arch="all">
      <minimum>
        <n-cpus>1</n-cpus>
        <ram>536870912</ram>
      </minimum>

      <recommended>
        <cpu>400000000</cpu>
        <ram>1073741824</ram>
        <storage>9663676416</storage>
      </recommended>
    </resources>

    <installer>
      <script id='http://redhat.com/rhel/kickstart/jeos'/>
      <script id='http://redhat.com/rhel/kickstart/desktop'/>
    </installer>
  </os>
</libosinfo>
